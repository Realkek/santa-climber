﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenTransitionImager : MonoBehaviour, IEventSub, IEventTrigger
{
    private Image _image;

    private void Start()
    {
        _image = GetComponent<Image>();
        StartCoroutine(DawnOfScreenCaster());
        Subscribe();
    }

    private IEnumerator DawnOfScreenCaster()
    {
        float colorAlphaCount = _image.color.a;
        while (_image.color.a > 0)
        {
            colorAlphaCount -= 0.01f;
            _image.color = new Color(0, 0, 0, colorAlphaCount);
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("SantaCollided", EnableGameOverState);
    }

    void EnableGameOverState()
    {
        StartCoroutine(SunsetScreenActivator());
    }

    private IEnumerator SunsetScreenActivator()
    {
        yield return new WaitForSeconds(0.5f);
        float colorAlphaCount = _image.color.a;
        while (_image.color.a < 1)
        {
            colorAlphaCount += 0.01f;
            _image.color = new Color(0, 0, 0, colorAlphaCount);
            yield return new WaitForSeconds(Time.deltaTime);
        }

        TriggerEvent("GameOver");
    }

    public void UnSubscribe()
    {
        throw new NotImplementedException();
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}