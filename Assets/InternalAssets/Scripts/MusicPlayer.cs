﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    private static MusicPlayer _musicPlayerInstance;

    private void Awake()
    {
        if (_musicPlayerInstance != null)
            Destroy(gameObject);
        else
        {
            _musicPlayerInstance = this;
        }

        DontDestroyOnLoad(gameObject);
    }
}