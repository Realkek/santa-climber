﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IEventSub
{
    private Animator _playerAnimator;
    private int _isMoveRight = 1;
    private float _moveSpeed = 8.5f;
    private bool _isEnablePlayerMoveNow = false;
    private bool _isJumpAvailable = false;
    private Rigidbody _rigidbody;
    private AudioSource _audioSource;
    [SerializeField] private AudioClip runningClip;
    [SerializeField] private AudioClip fallsClip;


    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _playerAnimator = transform.GetChild(0).GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody>();
        Subscribe();
    }

    IEnumerator DeferredMoveActivator()
    {
        yield return new WaitForSeconds(0.2f);
        _isEnablePlayerMoveNow = true;
    }

    void FirstEnablePlayerMove()
    {
        StartCoroutine(DeferredMoveActivator());
    }

    void EnablePlayerMove()
    {
        if (_isJumpAvailable)
        {
            _isJumpAvailable = false;
            _isEnablePlayerMoveNow = true;
        }
    }

    void DisablePlayerMove()
    {
        _audioSource.Stop();
        _audioSource.loop = true;
        _audioSource.PlayOneShot(runningClip);
        _playerAnimator.SetBool("isNeededJump", false);
        _isEnablePlayerMoveNow = false;
        _isJumpAvailable = true;
    }

    void PlayBraceHandAnimation()
    {
        _playerAnimator.SetBool("IsIdleToBracedHang", true);
    }

    void PlayJumpFromWallAnimation()
    {
        _playerAnimator.SetBool("isNeededJump", true);
        _audioSource.Stop();
        _audioSource.loop = false;
    }

    void PlayClimbingUpWallAnimation()
    {
        _audioSource.Stop();
        _playerAnimator.SetBool("isNeededSlower", true);
    }

    private void ResetClimbingUpWallAnimationState()
    {
        _playerAnimator.SetBool("isNeededSlower", false);
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("GamePlayStarted", FirstEnablePlayerMove);
        ManagerEvents.StartListening("GamePlayStarted", PlayBraceHandAnimation);
        ManagerEvents.StartListening("PlayerColliedWithWall", DisablePlayerMove);
        ManagerEvents.StartListening("PlayerTaped", PlayJumpFromWallAnimation);
        ManagerEvents.StartListening("CameraZoomReseted", PlayClimbingUpWallAnimation);
        ManagerEvents.StartListening("PlayerTaped", ResetClimbingUpWallAnimationState);
        ManagerEvents.StartListening("PlayerTaped", ChangeDirectionPlayerMove);
        ManagerEvents.StartListening("PlayerTaped", EnablePlayerMove);
        ManagerEvents.StartListening("SantaCollided", EnableDeathPlayerState);
    }


    private void Update()
    {
        MovePlayer();
    }

    void MovePlayer()
    {
        if (_isEnablePlayerMoveNow)
            switch (_isMoveRight)
            {
                case 0:
                    transform.Translate(-_moveSpeed * Time.deltaTime, 0, 0);
                    break;
                case 1:
                    transform.Translate(_moveSpeed * Time.deltaTime, 0, 0);
                    break;
            }
    }

    void EnableDeathPlayerState()
    {
        _audioSource.loop = false;
        _audioSource.Stop();
        _playerAnimator.SetBool("isdeadPlayer", true);
        _rigidbody.useGravity = true;
        _audioSource.PlayOneShot(fallsClip);
    }


    void ChangeDirectionPlayerMove()
    {
        if (_isJumpAvailable)
            switch (_isMoveRight)
            {
                case 0:
                    _isMoveRight = 1;
                    break;
                case 1:
                    _isMoveRight = 0;
                    break;
            }
    }

    public void UnSubscribe()
    {
        throw new NotImplementedException();
    }
}