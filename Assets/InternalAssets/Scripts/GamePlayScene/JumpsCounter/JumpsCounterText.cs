﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class JumpsCounterText : MonoBehaviour, IEventSub
{
    private TextMeshProUGUI _textMeshPro;
    private int _jumpsCount;

    private void Start()
    {
        _textMeshPro = GetComponent<TextMeshProUGUI>();
        Subscribe();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("PlayerTaped", CountJumps);
    }

    public void UnSubscribe()
    {
    }

    private void CountJumps()
    {
        _jumpsCount++;
        _textMeshPro.text = _jumpsCount.ToString();
    }
}