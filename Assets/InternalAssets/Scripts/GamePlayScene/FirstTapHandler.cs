﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstTapHandler : MonoBehaviour, IEventSub, IEventTrigger
{
    private void Awake()
    {
        Subscribe();
    }

    void StartGamePlay()
    {
        TriggerEvent("GamePlayStarted");
        gameObject.SetActive(false);
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("PlayerFirstTaped", StartGamePlay);
    }

    public void UnSubscribe()
    {
        ManagerEvents.StopListening("PlayerFirstTaped", StartGamePlay);
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}