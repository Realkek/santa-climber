﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandyCaneHelicopter : MonoBehaviour, ITick
{
    private void Start()
    {
        ManagerUpdate.AddTo(this);
    }

    public void Tick()
    {
        transform.Rotate(0, 30f, 0);
    }
}