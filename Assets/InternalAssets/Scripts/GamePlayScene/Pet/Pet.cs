﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Pet : WallsObjects
{
    private bool _isXPositionChangingNow;
    private bool _isYPositionChangingNow;
    private float _yPositionRandomCount;
    private float _xPositionRandomCount;
    private float _verticalMoveSpeed;
    private float _horizontalMoveSpeed;
    private const float VerticalMoveSpeedModifier = 1.2f;
    private const float HorizontalMovespeedModifier = 0.8f;
    private Animator _animatorController;

    private void Awake()
    {
        _animatorController = GetComponent<Animator>();
    }

    private void Start()
    {
        RandomYPosition();
        RandomXPosition();
    }

    private void Update()
    {
        if (_isYPositionChangingNow == false)
            ChangeYPosition();
        if (_isXPositionChangingNow == false)
            ChangeXPosition();
    }


    void ChangeYPosition()
    {
        _isYPositionChangingNow = true;

        MovePetVertical();
        if (_verticalMoveSpeed == -VerticalMoveSpeedModifier)
        {
            if (transform.position.y <= _yPositionRandomCount)
            {
                RandomYPosition();
            }
        }
        else if (_verticalMoveSpeed == VerticalMoveSpeedModifier)
        {
            if (transform.position.y >= _yPositionRandomCount)
            {
                RandomYPosition();
            }
        }

        _isYPositionChangingNow = false;
    }

    void ChangeXPosition()
    {
        _isXPositionChangingNow = true;

        MovePetHorizontal();
        if (_horizontalMoveSpeed == -HorizontalMovespeedModifier)
        {
            if (transform.position.x <= _xPositionRandomCount)
            {
                RandomXPosition();
            }
        }
        else if (_horizontalMoveSpeed == HorizontalMovespeedModifier)
        {
            if (transform.position.x >= _xPositionRandomCount)
            {
                RandomXPosition();
            }
        }

        _isXPositionChangingNow = false;
    }

    void MovePetVertical()
    {
        transform.Translate(0, _verticalMoveSpeed * Time.deltaTime, 0);
    }

    void MovePetHorizontal()
    {
        transform.Translate(_horizontalMoveSpeed * Time.deltaTime, 0, 0);
    }

    void RandomYPosition()
    {
        _yPositionRandomCount = Random.Range(CenterCam.y - 3, CenterCam.y + 5);
        if (transform.position.y > _yPositionRandomCount)
        {
            _verticalMoveSpeed = -VerticalMoveSpeedModifier;
        }
        else if (transform.position.y < _yPositionRandomCount)
        {
            _verticalMoveSpeed = VerticalMoveSpeedModifier;
        }
    }

    void RandomXPosition()
    {
        _xPositionRandomCount = Random.Range(CenterCam.x - 2f, CenterCam.x + 1.4f);
        if (transform.position.x > _xPositionRandomCount)
        {
            _horizontalMoveSpeed = -HorizontalMovespeedModifier;
        }
        else if (transform.position.x < _xPositionRandomCount)
        {
            _horizontalMoveSpeed = HorizontalMovespeedModifier;
        }
    }
}