﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinCollectingNotifyText : MonoBehaviour, IEventSub
{
    private TextMeshProUGUI _textMeshPro;
    private int _currentTextCount;
    private float _startFontSize;
    private int _countdown;
    private bool _isActiveNotifyDisabler;

    private void Start()
    {
        _textMeshPro = GetComponent<TextMeshProUGUI>();
        _startFontSize = _textMeshPro.fontSize;
        Subscribe();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("CoinReceived", ShowGoldCoinNotify);
    }

    public void UnSubscribe()
    {
    }

    private IEnumerator GoldCoinNotifyDisabler()
    {
        _isActiveNotifyDisabler = true;
        while (_countdown > 0)
        {
            yield return new WaitForSeconds(1);
            _countdown--;
        }

        _textMeshPro.text = ("+0");
        _textMeshPro.enabled = false;
        _isActiveNotifyDisabler = false;
    }


    private void Update()
    {
    }

    void ShowGoldCoinNotify()
    {
        _textMeshPro.enabled = true;
        _currentTextCount = int.Parse(_textMeshPro.text);
        _currentTextCount++;
        _textMeshPro.text = ($"+{_currentTextCount.ToString()}");
        CheckGoldCoinsCount();
        _countdown = 2;
        if (_isActiveNotifyDisabler == false)
            StartCoroutine(GoldCoinNotifyDisabler());
    }

    void CheckGoldCoinsCount()
    {
        if (_currentTextCount < 3)
        {
            _textMeshPro.color = Color.white;
            _textMeshPro.fontSize = _startFontSize;
        }
        else if (_currentTextCount < 6)
        {
            _textMeshPro.color = Color.yellow;
            _textMeshPro.fontSize *= 1.035f;
        }
        else if (_currentTextCount < 25)
        {
            _textMeshPro.color = Color.green;
            _textMeshPro.fontSize *= 1.02f;
        }
        else
        {
            _textMeshPro.color = Color.magenta;
        }
    }
}