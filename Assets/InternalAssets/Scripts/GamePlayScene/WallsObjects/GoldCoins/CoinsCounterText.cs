﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinsCounterText : MonoBehaviour, IEventSub
{
    private TextMeshProUGUI _textMeshPro;
    private int _coinsCount;

    private void Start()
    {
        _textMeshPro = GetComponent<TextMeshProUGUI>();
        _coinsCount = PlayerPrefs.GetInt("GoldCoins");
        _textMeshPro.text = _coinsCount.ToString();
        Subscribe();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("CoinReceived", CountGoldCoins);
    }

    public void UnSubscribe()
    {
    }

    private void CountGoldCoins()
    {
        _coinsCount++;
        _textMeshPro.text = _coinsCount.ToString();
        PlayerPrefs.SetInt("GoldCoins", _coinsCount);
    }
}