﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CoinsPacksElector : MonoBehaviour, IEventSub
{
    bool _isGamePlaying;

    private void Start()
    {
        Subscribe();
    }

    void ShowCoinsPack()
    {
        _isGamePlaying = true;
        StartCoroutine(CoinsPackChooseStarter());
    }

    private IEnumerator CoinsPackChooseStarter()
    {
        while (_isGamePlaying)
        {
            ChoseCoinsPackForShow();
            int howMuchSecondsWait = Random.Range(1, 4);
            yield return new WaitForSeconds(howMuchSecondsWait);
        }
    }

    void ChoseCoinsPackForShow()
    {
        int whichCoinsPackWillBeChosenNumber = Random.Range(1, transform.childCount);
        if (transform.GetChild(whichCoinsPackWillBeChosenNumber).gameObject.activeSelf == false)
            transform.GetChild(whichCoinsPackWillBeChosenNumber).gameObject.SetActive(true);
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("GamePlayStarted", ShowCoinsPack);
    }

    public void UnSubscribe()
    {
        throw new NotImplementedException();
    }
}