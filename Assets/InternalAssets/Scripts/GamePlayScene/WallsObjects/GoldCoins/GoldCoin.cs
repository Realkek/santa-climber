﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldCoin : MonoBehaviour, IEventTrigger
{
    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerCollector") || other.CompareTag("PetCollector"))
        {
            GetComponent<Animator>().SetBool("IsCoinReached", true);
            TriggerEvent("CoinReceived");
            _audioSource.Play();
            transform.GetChild(0).GetComponent<ParticleSystem>().Play();
        }
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}