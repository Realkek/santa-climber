﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldCoinPack : WallsObjects
{
    private void OnEnable()
    {
        transform.position = new Vector3(CenterCam.x, CenterCam.y + 16, transform.position.z);
        StartCoroutine(CurrentGoldCoinsPackDisabler());
    }

    private IEnumerator CurrentGoldCoinsPackDisabler()
    {
        yield return new WaitForSeconds(7);
        gameObject.SetActive(false);
    }
}