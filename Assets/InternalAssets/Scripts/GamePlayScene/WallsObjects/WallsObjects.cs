﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WallsObjects : MonoBehaviour, IEventSub, IScreenDataReceiver
{
    protected float _moveSpeed = 0.8f; //скорость анимации выдвижения стены
    protected Vector2 CenterCam; // центр камеры (это ее пивот)
    protected static float MinX, MaxX; // левая и правая границы
    private Vector3 _startWalls1Position;
    private float _widthCam; // ширина камеры


    private void Start()
    {
        _startWalls1Position = transform.localPosition;
        Subscribe();
    }

    private void GoStartPos()
    {
        transform.localPosition = _startWalls1Position;
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("secondChanceEarned", GoStartPos);
    }

    public void UnSubscribe()
    {
    }

    public void ReceiveDataAboutScreen()
    {
        if (Camera.main != null)
        {
            var main = Camera.main;
            _widthCam = main.orthographicSize *
                        main.aspect; // Получаем половину ширины камеры, путем умножения высоты на соотношение
            CenterCam = main.transform
                .position; // получаем центр камеры, т.к. пивот у камеры по умолчанию в центре, центром будет ее позиция
        }

        MinX = CenterCam.x - _widthCam; // левый край  (отнимаем от центра половину ширины)
        MaxX = CenterCam.x + _widthCam; // правый край (прибавляем к центру половину ширины)
    }
}