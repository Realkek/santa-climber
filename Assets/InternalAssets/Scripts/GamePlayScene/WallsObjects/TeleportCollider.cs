﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportCollider : MonoBehaviour
{
    public void TriggerEvent(string eventName)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }

    private void OnTriggerEnter(Collider other)
    {
        var nameWall = transform.name;
        TriggerEvent(nameWall == "LowerTeleportCollider"
            ? "FirstTeleportColliderReached"
            : "SecondTeleportColliderReached");
    }
}