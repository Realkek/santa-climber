﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowerWalls : MonoBehaviour, IWallTeleporter, IEventSub
{
    [SerializeField] private GameObject newWallPositionKeeper;
    [SerializeField] private GameObject startPlatform;
    private Transform _startWallPosition;

    private void Start()
    {
        Subscribe();
    }

    public void TeleportWall()
    {
        transform.position = newWallPositionKeeper.transform.position;
    }

    private void CheckStartPlatformState()
    {
        if (startPlatform.gameObject.activeSelf == true)
            startPlatform.transform.gameObject.SetActive(false);
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("FirstTeleportColliderReached", TeleportWall);
        ManagerEvents.StartListening("SecondTeleportColliderReached", TeleportWall);
        ManagerEvents.StartListening("FirstTeleportColliderReached", CheckStartPlatformState);
    }

    public void UnSubscribe()
    {
        ManagerEvents.StopListening("FirstTeleportColliderReached", TeleportWall);
        ManagerEvents.StopListening("FirstTeleportColliderReached", CheckStartPlatformState);
    }
}