﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpperWalls : MonoBehaviour, IWallTeleporter, IEventSub
{
    [SerializeField] private GameObject newWallPositionKeeper;

    private void Start()
    {
        Subscribe();
    }

    public void TeleportWall()
    {
        transform.position = newWallPositionKeeper.transform.position;
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("FirstTeleportColliderReached", TeleportWall);
        ManagerEvents.StartListening("SecondTeleportColliderReached", TeleportWall);
    }

    public void UnSubscribe()
    {
        ManagerEvents.StopListening("SecondTeleportColliderReached", TeleportWall);
    }
}