﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightWallObjects : WallsObjects, IWallSpotter
{
    private void Start()
    {
    }

    private void OnEnable()
    {
        ReceiveDataAboutScreen();
        float x = MaxX;
        var rigtWallPosition = transform.position;
        rigtWallPosition = new Vector3(x, rigtWallPosition.y, rigtWallPosition.z);
        transform.position = rigtWallPosition;
    }

    private void Update()
    {
        // AdjustmentWall();
    }

    public void AdjustmentWall()
    {
        var position = transform.position;
        float x = Mathf.Clamp(position.x - (_moveSpeed * Time.deltaTime), MaxX, MaxX + 0.7f);
        position = new Vector3(x, position.y, position.z);
        transform.position = position;
    }
}