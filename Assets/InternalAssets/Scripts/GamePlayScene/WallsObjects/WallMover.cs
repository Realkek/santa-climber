﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WallMover : MonoBehaviour, ITick, IEventSub
{
    private float _wallMoveSpeed = 9f;

    private bool _isEnableMoveNow = false;

    // [SerializeField] private EventsCollection secondChanceEarned;
    // [SerializeField] EventsCollection arrowCollided;
    // [SerializeField] Score scoreField;
    private Vector3 _startWallPos;
    // private int _previousScoreValue;
    // private AudioSource _audioSource;
    // private string _isSoundButtonState;

    private void Start()
    {
        _startWallPos = transform.position;
        GoPlay();
        Subscribe();
        // _audioSource = GetComponent<AudioSource>();
        // _isSoundButtonState = PlayerPrefs.GetString("isSoundButtonState");
        // if (_isSoundButtonState == "ON")
        // {
        //     _audioSource.mute = false;
        // }
        // else
        // {
        //     _audioSource.mute = true;
        // }
    }

    void GoPlay()
    {
        transform.position = _startWallPos;
        ManagerUpdate.AddTo(this);
    }

    void StopWallMovement()
    {
        ManagerUpdate.RemoveFrom(this);
    }

    void MoveWall()
    {
        if (_isEnableMoveNow)
            transform.Translate(0, -_wallMoveSpeed * Time.deltaTime, 0);
    }

    void EnableMoveWallsObjects()
    {
        StartCoroutine(MoveActivator());
    }

    IEnumerator MoveActivator()
    {
        yield return new WaitForSeconds(0.3f);
        _isEnableMoveNow = true;
    }

    public void Tick()
    {
        MoveWall();
    }

    private void DecreaseWallMoveSpeedByCameraZoomReseted()
    {
        _wallMoveSpeed = 7f;
    }

    private void IncreaseWallMoveSpeedByCameraZoomed()
    {
        _wallMoveSpeed = 9f;
    }

    private void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    private void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("GamePlayStarted", EnableMoveWallsObjects);
        StartListening("SantaCollided", StopWallMovement);
        StartListening("SecondChanceEarned", GoPlay);
        StartListening("CameraZoomReseted", DecreaseWallMoveSpeedByCameraZoomReseted);
        StartListening("CameraZoomed", IncreaseWallMoveSpeedByCameraZoomed);
    }

    public void UnSubscribe()
    {
        StopListening("GamePlayStarted", EnableMoveWallsObjects);
        StopListening("SantaCollided", StopWallMovement);
        StopListening("SecondChanceEarned", GoPlay);
        StopListening("CameraZoomReseted", DecreaseWallMoveSpeedByCameraZoomReseted);
        StopListening("CameraZoomed", IncreaseWallMoveSpeedByCameraZoomed);
    }
}