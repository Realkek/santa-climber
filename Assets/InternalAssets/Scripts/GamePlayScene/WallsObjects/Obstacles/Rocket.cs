﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Rocket : WallsObjects
{
    private float _rocketMoveSpeed = 7.9f;

    private void OnEnable()
    {
        float randomXPosition = Random.Range(CenterCam.x - 2, CenterCam.x + 2f);
        transform.position = new Vector3(randomXPosition, CenterCam.y + 50, transform.position.z);
    }

    void MoveRocket()
    {
        transform.Translate(0, _rocketMoveSpeed * Time.deltaTime, 0);
    }

    private void Update()
    {
        MoveRocket();
    }
}