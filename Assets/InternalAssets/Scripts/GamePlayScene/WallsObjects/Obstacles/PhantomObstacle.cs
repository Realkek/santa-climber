﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhantomObstacle : MonoBehaviour, IEventTrigger
{
    private static float _startPhantomObstacleInstancePositionY;
    [SerializeField] private GameObject rightObstacles;
    [SerializeField] private GameObject leftObstacles;

    private void Start()
    {
        _startPhantomObstacleInstancePositionY = transform.position.y;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("ObstacleReachesCollider"))
        {
            transform.position =
                new Vector3(transform.position.x, _startPhantomObstacleInstancePositionY, transform.position.z);

            if (leftObstacles.transform.childCount > 0 || rightObstacles.transform.childCount > 0)
            {
                int activeObstaclesNumber = 0;
                for (int i = 0; i < leftObstacles.transform.childCount; i++)
                {
                    if (leftObstacles.transform.GetChild(i).gameObject.activeSelf)
                        activeObstaclesNumber++;
                    if (activeObstaclesNumber != 0)
                        return;
                }

                for (int i = 0; i < rightObstacles.transform.childCount; i++)
                {
                    if (rightObstacles.transform.GetChild(i).gameObject.activeSelf)
                        activeObstaclesNumber++;
                    if (activeObstaclesNumber != 0)
                        return;
                }
            }

            TriggerEvent("PhantomObstacleReachesColliderReached");
        }
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}