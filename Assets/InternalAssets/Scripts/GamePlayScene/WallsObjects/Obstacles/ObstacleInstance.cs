﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleInstance : MonoBehaviour, IEventTrigger
{
    private static float _startObstacleInstancePositionY;

    // private void Start()
    // {
    //     _startObstacleInstancePositionY = transform.position.y;
    // }


    private void OnEnable()
    {
        _startObstacleInstancePositionY = 11.04f; // temp hardcoded
        ResetObstacleToStartPosition();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("ObstacleReachesCollider"))
        {
            TriggerEvent("ObstacleReachesColliderReached");
            ResetObstacleToStartPosition();
            gameObject.SetActive(false);
        }

        if (other.CompareTag("PlayerCollector"))
            TriggerEvent("SantaCollided");
    }

    void ResetObstacleToStartPosition()
    {
        transform.position =
            new Vector3(transform.position.x, _startObstacleInstancePositionY, transform.position.z);
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}