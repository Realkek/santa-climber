﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ObstaclesElector : MonoBehaviour, IEventSub
{
    [SerializeField] private GameObject rightObstacles;
    [SerializeField] private GameObject leftObstacles;

    private void Start()
    {
        Subscribe();
    }

    void ChooseAndShowObstaclesSide()
    {
        leftObstacles.SetActive(false);
        rightObstacles.SetActive(false);
        int obstacleSideNumber = Random.Range(1, 3);
        if (obstacleSideNumber == 1)
            leftObstacles.SetActive(true);
        else if (obstacleSideNumber == 2)
        {
            rightObstacles.SetActive(true);
        }
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("ObstacleReachesColliderReached", ChooseAndShowObstaclesSide);
        ManagerEvents.StartListening("PhantomObstacleReachesColliderReached", ChooseAndShowObstaclesSide);
    }

    public void UnSubscribe()
    {
        throw new System.NotImplementedException();
    }
}