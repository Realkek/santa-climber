﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Obstacles : MonoBehaviour
{
    private void OnEnable()
    {
        ActivateObstaclesRandom();
    }


    private IEnumerator ObstaclesRandomEnabler()
    {
        yield return new WaitForSeconds(0.1f);

        int obstacleNumber = Random.Range(0, transform.childCount);
        int isEnableObstacle = Random.Range(0, 5);

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        if (isEnableObstacle != 0)
            transform.GetChild(obstacleNumber).gameObject.SetActive(true);
    }

    void ActivateObstaclesRandom()
    {
        StartCoroutine(ObstaclesRandomEnabler());
    }
}