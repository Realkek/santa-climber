﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightWallCollider : MonoBehaviour, IEventTrigger
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            TriggerEvent("PlayerColliedWithWall");
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}