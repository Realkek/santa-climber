﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftWallObjects : WallsObjects, IWallSpotter
{
    private void Start()
    {
    }

    private void OnEnable()
    {
        ReceiveDataAboutScreen();
        var leftWallPosition = transform.position;
        float x = MinX;

        leftWallPosition = new Vector3(x, leftWallPosition.y, leftWallPosition.z);
        transform.position = leftWallPosition;
    }

    private void Update()
    {
        // AdjustmentWall();
    }

    public void AdjustmentWall()
    {
        var position = transform.position;
        float x = Mathf.Clamp(position.x + (_moveSpeed * Time.deltaTime), MinX - 0.7f, MinX);
        position = new Vector3(x, position.y, position.z);
        transform.position = position;
    }
}