﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundsElector : MonoBehaviour
{
    [SerializeField] private GameObject oneBackground;
    [SerializeField] private GameObject secondBackground;
    [SerializeField] private GameObject thirdBackground;

    void Start()
    {
        ChooseBackground();
    }

    void ChooseBackground()
    {
        oneBackground.SetActive(false);
        secondBackground.SetActive(false);
        thirdBackground.SetActive(false);
        var whichBackgroundToChosenNumber = Random.Range(1, 4);
        switch (whichBackgroundToChosenNumber)
        {
            case 1:
                oneBackground.SetActive(true);
                break;
            case 2:
                secondBackground.SetActive(true);
                break;
            case 3:
                thirdBackground.SetActive(true);
                break;
        }
    }
}