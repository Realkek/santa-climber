﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SantaRotator : MonoBehaviour, IEventSub
{
    private int _isMoveRight = 0;
    private bool _isRotateAvailable = false;

    private void Start()
    {
        Subscribe();
    }

    void RotatePlayer()
    {
        _isRotateAvailable = false;
        switch (_isMoveRight)
        {
            case 0:
                _isMoveRight = 1;
                StartCoroutine(PlayerLeftRotator());
                break;
            case 1:
                _isMoveRight = 0;
                StartCoroutine(PlayerRightRotator());
                break;
        }
    }

    private void EnablePlayerRotate()
    {
        _isRotateAvailable = true;
    }

    private IEnumerator PlayerRightRotator()
    {
        int i = 0;
        while (i < 9)
        {
            transform.Rotate(0, 20f, 0);
            i++;
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    private IEnumerator PlayerLeftRotator()
    {
        int i = 0;
        while (i < 9)
        {
            transform.Rotate(new Vector3(0, -20f, 0));
            i++;
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("PlayerColliedWithWall", EnablePlayerRotate);
        ManagerEvents.StartListening("PlayerTaped", RotatePlayer);
    }


    public void UnSubscribe()
    {
    }
}