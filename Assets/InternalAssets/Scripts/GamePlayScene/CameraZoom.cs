﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour, IEventSub, IEventTrigger
{
    private bool _isZoomerActive = false;
    private bool _isReseterActive = false;
    private bool _isJumpingPlayer;

    private void Start()
    {
        Subscribe();
    }

    private void EnableFirstCameraZoom()
    {
        StartCoroutine(FirstCameraZoomer());
    }

    void EnableJumpingPlayerState()
    {
        _isJumpingPlayer = true;
    }

    void DisableJumpingPlayerState()
    {
        _isJumpingPlayer = false;
    }

    private IEnumerator FirstCameraZoomer()
    {
        int i = 0;
        yield return new WaitForSeconds(0.1f);
        while (i < 15)
        {
            i++;
            Camera.main.orthographicSize += 0.01f;
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    private IEnumerator CameraZoomer()
    {
        {
            _isReseterActive = false;
            _isZoomerActive = true;
            TriggerEvent("CameraZoomed");
            while (_isZoomerActive && _isReseterActive == false)
            {
                if (Camera.main.orthographicSize >= 5.35f)
                {
                    _isZoomerActive = false;
                    break;
                }

                Camera.main.orthographicSize += 0.03f;
                yield return new WaitForSeconds(Time.deltaTime);
            }
        }
        _isZoomerActive = false;
    }

    private IEnumerator CameraZoomReseter()
    {
        yield return new WaitForSeconds(1.5f);
        TriggerEvent("CameraZoomReseted");
        _isReseterActive = true;
        _isZoomerActive = false;
        while (_isReseterActive && _isZoomerActive == false && _isJumpingPlayer == false)
        {
            if (Camera.main.orthographicSize <= 4.9f)
            {
                _isReseterActive = false;
                break;
            }

            Camera.main.orthographicSize -= 0.01f;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        _isReseterActive = false;
    }

    private void EnableCameraZoom()
    {
        StartCoroutine(CameraZoomer());
    }

    private void ResetCameraZoom()
    {
        StartCoroutine(CameraZoomReseter());
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("GamePlayStarted", EnableFirstCameraZoom);
        ManagerEvents.StartListening("PlayerTaped", EnableCameraZoom);
        ManagerEvents.StartListening("PlayerTaped", EnableJumpingPlayerState);
        ManagerEvents.StartListening("PlayerColliedWithWall", ResetCameraZoom);
        ManagerEvents.StartListening("PlayerColliedWithWall", DisableJumpingPlayerState);
    }


    public void UnSubscribe()
    {
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}