﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePlayHandler : MonoBehaviour, IEventSub
{
    private void Start()
    {
        Subscribe();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("GameOver", HandleGameOver);
    }

    void HandleGameOver()
    {
        SceneManager.LoadScene("GamePlay");
    }

    public void UnSubscribe()
    {
        throw new System.NotImplementedException();
    }
}