﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapToPlayCollider : MonoBehaviour, IEventTrigger, IEventSub
{
    private bool _isFirstTap = true;
    private bool _isTapAvailable = true;
    private AudioSource _audioSource;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        Subscribe();
    }

    private void OnMouseDown()
    {
        if (_isTapAvailable)
        {
            _isTapAvailable = false;
            TriggerEvent(_isFirstTap ? "PlayerFirstTaped" : "PlayerTaped");
            if (!_isFirstTap)
                _audioSource.Play();
            _isFirstTap = false;
            StartCoroutine(TapEnabler());
        }
    }

    private IEnumerator TapEnabler()
    {
        yield return new WaitForSeconds(0.52f);
        _isTapAvailable = true;
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("SantaCollided", DisableTapToPlayCollider);
    }

    private void DisableTapToPlayCollider()
    {
        gameObject.SetActive(false);
    }

    public void UnSubscribe()
    {
        throw new NotImplementedException();
    }
}