﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowTrail : MonoBehaviour, IEventSub
{
    [SerializeField] private GameObject snowTrailPlayerPosition;
    private ParticleSystem _snowTrailParticle;

    private void Awake()
    {
        Subscribe();
        _snowTrailParticle = GetComponent<ParticleSystem>();
    }


    public void Subscribe()
    {
        ManagerEvents.StartListening("PlayerTaped", ShowSnowTrailParticleEffect);
    }

    private void ShowSnowTrailParticleEffect()
    {
        transform.position = snowTrailPlayerPosition.transform.position;
        _snowTrailParticle.Play();
    }

    public void UnSubscribe()
    {
        throw new NotImplementedException();
    }
}