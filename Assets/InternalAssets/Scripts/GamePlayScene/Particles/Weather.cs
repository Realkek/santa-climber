﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Weather : MonoBehaviour, IEventSub
{
    [SerializeField] private GameObject snowParticleSystem;
    [SerializeField] private GameObject rainParticleSystem;
    private int _whichParticleSystemWillBeShown;

    private void Start()
    {
        Subscribe();
        _whichParticleSystemWillBeShown = Random.Range(1, 3);
        if (_whichParticleSystemWillBeShown == 1)
            snowParticleSystem.SetActive(true);
        else if (_whichParticleSystemWillBeShown == 2)
        {
            rainParticleSystem.SetActive(true);
        }
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("SantaCollided", EnableGameOverState);
    }

    private void EnableGameOverState()
    {
        if (_whichParticleSystemWillBeShown == 1)
        {
            var module = snowParticleSystem.GetComponent<ParticleSystem>().main;
            module.loop = false;
            module.startLifetime = 0.5f;
        }
        else if (_whichParticleSystemWillBeShown == 2)
        {
            var module = rainParticleSystem.GetComponent<ParticleSystem>().main;
            module.loop = false;
            module.startLifetime = 0.5f;
        }
    }

    public void UnSubscribe()
    {
        throw new NotImplementedException();
    }
}