﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapToPlayText : MonoBehaviour, IEventSub
{
    private void Start()
    {
        Subscribe();
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("PlayerFirstTaped", HideTapToPlayText);
    }

    private void HideTapToPlayText()
    {
        gameObject.SetActive(false);
    }

    public void UnSubscribe()
    {
        throw new System.NotImplementedException();
    }
}